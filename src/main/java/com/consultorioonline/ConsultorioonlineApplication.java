package com.consultorioonline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsultorioonlineApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsultorioonlineApplication.class, args);
    }

}
