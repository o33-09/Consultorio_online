package com.consultorioonline.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.consultorioonline.models.Medico;

public interface MedicoRepository extends JpaRepository<Medico, Integer> {

}
