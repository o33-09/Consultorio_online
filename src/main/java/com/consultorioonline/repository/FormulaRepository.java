package com.consultorioonline.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.consultorioonline.models.Formula;

public interface FormulaRepository extends JpaRepository<Formula,Integer> {
}
