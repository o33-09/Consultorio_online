package com.consultorioonline.repository;

import com.consultorioonline.models.Paciente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PacienteRepository extends JpaRepository<Paciente,Integer> {

}
