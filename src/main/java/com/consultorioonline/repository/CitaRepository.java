package com.consultorioonline.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.consultorioonline.models.Cita;

public interface CitaRepository extends JpaRepository<Cita,Integer> {
}
