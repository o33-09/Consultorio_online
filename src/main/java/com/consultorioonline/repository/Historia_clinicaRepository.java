package com.consultorioonline.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.consultorioonline.models.Historia_clinica;

public interface Historia_clinicaRepository extends JpaRepository<Historia_clinica,Integer> {
}
