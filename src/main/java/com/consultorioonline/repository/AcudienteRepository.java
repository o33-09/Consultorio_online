package com.consultorioonline.repository;

        import com.consultorioonline.models.Acudiente;
        import org.springframework.data.jpa.repository.JpaRepository;

public interface AcudienteRepository extends JpaRepository<Acudiente,Integer> {
}