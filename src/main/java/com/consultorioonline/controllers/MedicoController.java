package com.consultorioonline.controllers;
import com.consultorioonline.models.Medico;
import com.consultorioonline.services.MedicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class MedicoController {

    @Autowired
    MedicoService service;

    @GetMapping({"/medico/list"})
    public String Listar(Model model) {
        model.addAttribute("Medico",service.Listar());
        return "/medico/list";
    }

    @GetMapping({"/medico/create"})
    public String Nuevo(Model model) {
        Medico medico = new Medico();
        model.addAttribute("medico", medico);
        model.addAttribute("Medico",service.Listar());
        return "/medico/create";
    }

    @PostMapping({"/medico/guardar"})
    public String Guardar(@ModelAttribute("medico") Medico medico){
        service.Guardar(medico);
        return "redirect:/medico/list";
    }

    @GetMapping({"/medico/edit/{id}"})
    public String Editar(@PathVariable int id,Model model){
        Medico teacher= service.BuscarById(id);
        model.addAttribute("teacher",teacher);
        return "/medico/create";
    }

    @GetMapping({"/medico/delete/{id}"})
    public String Eliminar(@PathVariable int id){
        service.Eliminar(id);
        return "redirect:/medico/list";
    }
}