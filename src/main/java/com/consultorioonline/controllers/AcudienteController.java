package com.consultorioonline.controllers;
import com.consultorioonline.models.Acudiente;
import com.consultorioonline.services.AcudienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class AcudienteController {

    //Mapear la ruta para mostrar el listado de Formulas de pacientes
    @Autowired
    AcudienteService service;

    //Mapear la ruta para mostrar el listado de acudientes
    @GetMapping({"/acudiente/list"})
    public String Listar (Model model) {
        model.addAttribute("acudiente",service.Listar());
        return "/acudiente/list";
    }

    //Ruta para formulario nuevo
    @GetMapping({"/acudiente/create"})
    public String Nuevo (Model model){
        Acudiente acudiente = new Acudiente();
        model.addAttribute("acudiente",acudiente);
        return "/acudiente/create";
    }

    //Ruta para guardar y actualizar
    @PostMapping ({"/acudiente/guardar"})
    public String Guardar (@ModelAttribute("acudiente")Acudiente acudiente){
        service.Guardar(acudiente);
        return "redirect:/acudiente/list";
    }
}
