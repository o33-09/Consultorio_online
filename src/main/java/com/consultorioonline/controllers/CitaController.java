package com.consultorioonline.controllers;

import com.consultorioonline.services.CitaServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller

public class CitaController {
    @Autowired
    CitaServices service;

    @GetMapping({"/cita/list"})
    public String Listar(Model model) {
        model.addAttribute("cita",service.Listar());
        return "/cita/list";
    }

}
