package com.consultorioonline.controllers;

import com.consultorioonline.models.Paciente;
import com.consultorioonline.services.PacienteServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class PacienteController {
    //mapear la ruta para mostrar el listado del consultorio
    @Autowired
    PacienteServicio service;
    @GetMapping({"/paciente/list1"})// llama a la lista de pacientes
    public String Listar(Model model){
        model.addAttribute("paciente", service.Listar());
        return "/paciente/list1";
    }

    //ruta para formulario nuevo
    @GetMapping({"/paciente/create"})
    public String Nuevo(Model model){
        Paciente paciente= new Paciente();
        model.addAttribute("paciente",paciente);
        return "/paciente/create";
    }

    //ruta para guardar
    @PostMapping({"/paciente/guardar"})
    public String Guardar(@ModelAttribute("paciente") Paciente paciente){
        service.Guardar(paciente);
        return "redirect:/paciente/list1";
    }
}
