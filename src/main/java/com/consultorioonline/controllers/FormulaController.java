package com.consultorioonline.controllers;

import com.consultorioonline.models.Formula;
import com.consultorioonline.services.FormulaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class FormulaController {

    //Mapear la ruta para mostrar el listado de Formulas de pacientes
    @Autowired
    FormulaService service;
    @GetMapping({"/pacientes/formulasList"})
    public String Listar (Model model) {
        model.addAttribute("formulas",service.Listar());
        return "/pacientes/formulasList";
    }

    @GetMapping({"/pacientes/createFormula"})
    public String Nuevo (Model model) {
        Formula formula = new Formula();
        model.addAttribute("formula",formula);
        return "/pacientes/createFormula";
    }

    @PostMapping({"/formula/guardar"})
    public String Guardar(@ModelAttribute("formula") Formula formula) {
        service.Guardar(formula);
        return "redirect:/pacientes/formulasList";
    }

}
