package com.consultorioonline.controllers;
import com.consultorioonline.models.Historia_clinica;
import com.consultorioonline.services.Historia_clinicaServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class Historia_clinicaController {

    @Autowired
    Historia_clinicaServices service;

    @GetMapping({"/historia_cli/list"})
    public String Listar(Model model) {
        model.addAttribute("historia",service.Listar());
        return "/historia_clinica/list";
    }

    //ruta formulario nuevo
    @GetMapping({"/historia_cli/nuevo"})
    public String Nuevo(Model model){
        Historia_clinica historia_clinica=new Historia_clinica();
        model.addAttribute("historia_clinica",historia_clinica);
        return "/historia_clinica/create";
    }
    // ruta para guardar y actualizar
    @PostMapping({"/historia_clinica/guardar"})
    public String Guardar(@ModelAttribute("Historia_clinica") Historia_clinica historia_clinica){
        service.Guardar(historia_clinica);
        return "redirect:/historia_cli/list";
    }


    //Metodo para editar
    @GetMapping({"/historia_clinica/edit/{id}"})
    public String Editar(@PathVariable int id, Model model){
        Historia_clinica historia_clinica= service.BuscarById(id);
        model.addAttribute("historia_clinica",historia_clinica);
        return "/historia_clinica/create";
    }

    @GetMapping({"/historia_clinica/delete/{id}"})
    public String Eliminar(@PathVariable int id){
        service.Eliminar(id);
        return "redirect:/historia_cli/list";
    }

}
