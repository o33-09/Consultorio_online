package com.consultorioonline.models;

import lombok.Getter;
import lombok.Setter;


import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name="paciente")
public class Paciente {
    //atributos
    @Id
    private int id_documento_paciente;
    private String nombre;
    private String apellido;
    private String fecha_nacimiento;
    private int telefono;
    private String direccion;
    private String correo;
    private int id_acudiente;


}
