package com.consultorioonline.models;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name="medico")
public class Medico {
    @Id
    private int id_tarjeta_profesional;
    private String nombre;
    private String apellido;
    private int id_especialidad;
    private String telefono;
    private String correo;
}
