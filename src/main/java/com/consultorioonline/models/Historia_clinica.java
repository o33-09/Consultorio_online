package com.consultorioonline.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;

@Getter
@Setter
@Entity
@Table(name = "historia_clinica")


public class Historia_clinica {
    @Id
    private int id_numero_historia;
    private String descripcion;
    //@Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    private String hora;
    private int id_medico;
    private int id_formula;
    private int id_paciente;
    private int diagnostico;

}
