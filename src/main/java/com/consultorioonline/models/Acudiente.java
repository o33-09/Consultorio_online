package com.consultorioonline.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name="acudiente")
public class Acudiente {

    @Id
    private int id_documento_acu;
    private String nombre;
    private String apellido;
    private String parentezco;
    private int telefono;
    private String direccion;
    private String correo;
}