package com.consultorioonline.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name="formula")
public class Formula {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_formula;
    private int id_medicamento;
    private String indicaciones;
    private int cantidad;
    private int id_cita;

}
