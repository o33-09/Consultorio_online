package com.consultorioonline.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;


@Getter
@Setter
@Entity
@Table(name = "cita")

public class Cita {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_cita;
    private Date fecha_cita;
    private String hora_cita;
    private int id_medico;
    private int id_paciente;
    private String plataforma;
    private String observacion;

}
