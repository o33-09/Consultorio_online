package com.consultorioonline.services;

import com.consultorioonline.models.Acudiente;
import com.consultorioonline.repository.AcudienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class AcudienteService {

    @Autowired
    AcudienteRepository repository;

    public List<Acudiente> Listar() {
        return repository.findAll();
    }

    public Acudiente Guardar(Acudiente acudiente) {
        return repository.save(acudiente);
    }
}