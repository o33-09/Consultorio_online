package com.consultorioonline.services;

import com.consultorioonline.models.Formula;
import com.consultorioonline.repository.FormulaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FormulaService {

    @Autowired
    FormulaRepository repository;

    public List<Formula> Listar(){
        return repository.findAll();
    }

    public Formula Guardar (Formula formula)
    {
        return repository.save(formula);
    }
}
