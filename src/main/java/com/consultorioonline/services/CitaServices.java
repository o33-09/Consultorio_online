package com.consultorioonline.services;

import com.consultorioonline.models.Cita;
import com.consultorioonline.repository.CitaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class CitaServices {
    @Autowired
    CitaRepository repository;
    public List<Cita> Listar(){
        return repository.findAll();
    }
}
