package com.consultorioonline.services;
import com.consultorioonline.models.Medico;
import com.consultorioonline.repository.MedicoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
@Service
public class MedicoService {
    @Autowired
    MedicoRepository repository;

    public List<Medico> Listar(){
        return repository.findAll();
    }

    public Medico Guardar(Medico medico) {
        return repository.save(medico);
    }

    public Medico BuscarById(Integer id){
        return repository.findById(id).get();
    }

    public void Eliminar(Integer id){
        repository.deleteById(id);
    }
}