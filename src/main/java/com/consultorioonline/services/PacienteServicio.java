package com.consultorioonline.services;

import com.consultorioonline.models.Paciente;
import com.consultorioonline.repository.PacienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PacienteServicio {
    @Autowired
    PacienteRepository repository;

    public List<Paciente> Listar(){
        return repository.findAll();
    }

    public Paciente Guardar(Paciente paciente){
        return repository.save(paciente);
    }
}
