package com.consultorioonline.services;

import com.consultorioonline.models.Historia_clinica;
import com.consultorioonline.repository.Historia_clinicaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class Historia_clinicaServices {
    @Autowired
    Historia_clinicaRepository repository;
    public List<Historia_clinica> Listar(){
        return repository.findAll();
    }

    public Historia_clinica Guardar(Historia_clinica historia_clinica){
        return repository.save(historia_clinica);
    }

    //Read
    //Metodo de Buscar por Id
    public Historia_clinica BuscarById(Integer id){
        return repository.findById(id).get();
    }
    //Delete
    //Metodo de Eliminar
    public void Eliminar(Integer id){
        repository.deleteById(id);
    }
}
